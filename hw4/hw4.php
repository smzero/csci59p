<html>
<body>
<?php
    echo "<strong><br><h1>Homework 4</h1><br><hr></strong></br>";
    /* Display first and last name */
    echo "</h1>Scott Meadows</h1><br/>";
    echo "<br/>";
    //Tell PHP where to find the file we will be referencing.
    $hwFile = "hw3.txt";
    //read the file into an array.
    $lines = file($hwFile) or die("Error! The script encountered an error while trying to locate the file: ".strval($hwFile));
    //define a table to make the output look pretty.
    echo "<table class='tg'>";
    //split the script to inject some CSS style... Again, to make things pretty.
?>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<?php
    //loop through and write an associative array called newArray[].
    foreach ($lines as $line) {
         //define the first column.
         $Key = substr($line, 0, 3);
         //define the second column.
         $Value = substr($line, 3);
         //assign the data to an associative array.
         $newArray[$Key] = $Value;
    }
    //sort the new associative array by the key.
    ksort($newArray);
    //Display the data via repetition.
    foreach ($newArray as $key => $value) {
         //create table entries for each column.
         echo "<tr>";
         echo "<th class='tg-0lax'> ".$key." </th>";
         echo "<th class='tg-0lax'> ".$value." </th>";
         echo "</tr>";
    }
?>
</table>
</body>
</html>